import { Component, OnInit } from '@angular/core';
import {Director} from '../models/director.model';
import {Observable} from 'rxjs';
import {DirectorService} from '../services/director.service';

@Component({
  selector: 'app-director-list',
  templateUrl: './director-list.component.html',
  styleUrls: ['./director-list.component.css']
})
export class DirectorListComponent implements OnInit {
  directorList: Observable<Array<Director>>;
  alphabet: Array<string> = Array.from('ABCDEFGHIJKLMNOPQRSTUVWXYZ');

  directorInfo?: Director;

  constructor(directorService: DirectorService) {
    this.directorList = directorService.getDirectors(); 
  }

  showDirectorInfo(director: Director) {
    this.directorInfo = director;
  }

  ngOnInit(): void {
  }

}
