import { Component, OnInit } from '@angular/core';
import { Film } from '../models/film.model';
import { WatchlistService } from '../services/watchlist.service';

@Component({
  selector: 'app-watchlist',
  templateUrl: './watchlist.component.html',
  styleUrls: ['./watchlist.component.css']
})
export class WatchlistComponent implements OnInit {

  watchlist: Array<Film>;

  constructor(private watchlistService: WatchlistService) {}

  ngOnInit(): void {
    this.watchlistService.getWatchlist().subscribe(data => {
      this.watchlist = data;
    });
  }

  deleteFromWatchlist(event): void {
    this.watchlist = this.watchlist.filter(item => item._id !== event);
  }
}
