import { Component, OnInit } from '@angular/core';
import {Actor} from '../models/actor.model';
import {Observable} from 'rxjs';
import {ActorService} from '../services/actor.service';

@Component({
  selector: 'app-actor-list',
  templateUrl: './actor-list.component.html',
  styleUrls: ['./actor-list.component.css']
})
export class ActorListComponent implements OnInit {
  actorList: Observable<Array<Actor>>;
  alphabet: Array<string> = Array.from('ABCDEFGHIJKLMNOPQRSTUVWXYZ');

  actorInfo?: Actor;

  constructor(actorService: ActorService) {
    this.actorList = actorService.getActors(); 
  }

  showActorInfo(actor: Actor) {
    this.actorInfo = actor;
  }

  ngOnInit(): void {
  }

}
