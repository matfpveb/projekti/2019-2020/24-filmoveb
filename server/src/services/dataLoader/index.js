require('dotenv').config({path: './src/.env'});
require('../dbConfig');
const mongoose = require('mongoose');
const Movie = require('../../api/movie/movie.model');
const Actor = require('../../api/actor/actor.model');
const Director = require('../../api/director/director.model');
const User = require('../../api/user/user.model');
const { readAllMovies, readAllDataInDir, getMoviePoster } = require('./loadData');
const logger = require('../logger');

const createMovieObject = async (movie) => {

  const directorQuery = movie.director === 'string' ? movie.director : { $in: movie.director };
  const movieDirectors = await Director.find({ name: directorQuery });
  const movieDirectorsIds = movieDirectors.map(item => item._id);
  Object.assign(movie, {directors: movieDirectorsIds.length ? movieDirectorsIds : null});

  const movieActors = await Actor.find({ name: { $in: movie.actors }});
  const movieActorsIds = movieActors.map(item => item._id);
  Object.assign(movie, {actors: movieActorsIds.length ? movieActorsIds : null});
  
  const poster = await getMoviePoster(movie.name);
  Object.assign(movie, { poster });

  return movie;
}

const main = async () => {
  const [movies, actors, directors] = await Promise.all([
    readAllMovies('./src/services/dataLoader/data/movies/'),
    readAllDataInDir('./src/services/dataLoader/data/actors/'),
    readAllDataInDir('./src/services/dataLoader/data/directors/')
  ]);

  // add test user
  await User.create({ 
    email: "test@test.com",
    password: "testtest",
    userName: "test" 
  });

  for(const actor of actors) {
    await new Actor(actor).save();
  }

  logger.info(`Added ${actors.length} actors to db`);

  for(const director of directors) {
    await new Director(director).save();
  }

  logger.info(`Added ${directors.length} directors to db`);

  await Promise.all(movies.map(async (item) => {
    const movie = await createMovieObject(item);
    await new Movie(movie).save();
  }));

  logger.info(`Added ${movies.length} movies to db`);

  logger.info('Successfully imported data in db');

  mongoose.connection.close();

}

main();
