import {Actor} from './actor.model';
import {Director} from './director.model';

export class Film {
    _id : string;
    name: string; 
    year: number;
    runtime: number;
    categories: Array<string>;
    release_date: Date;
    directors: Array<Director>;
    actors: Array<Actor>;
    storyline: string;
    poster: string;
    ratings: any;
}
