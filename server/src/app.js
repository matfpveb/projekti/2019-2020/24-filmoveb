const {resolve} = require('path')

require('dotenv').config({path: resolve(__dirname,"./.env")});

const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const cors = require('cors');
const passport = require('passport');

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(morgan("dev"));

app.use(passport.initialize());
app.use(passport.session());
require('./services/passport')(passport);

// Database configuration
require('./services/dbConfig');

// Routes configuration
require('./routes')(app);

module.exports = app;
