const http = require('http');
const app = require('./app');
const logger = require('./services/logger');

const port = '3000';
app.set('port', port);

const server = http.createServer(app);

process.on("unhandledRejection", (reason, promise) => {
  logger.info(`Unhandled Promise rejection: \n${reason.stack || reason}`);
});

server.listen(port);
server.on('listening', () => {
  const addr = server.address();
  const bind = typeof addr === 'string' ? `pipe ${addr}` : `port ${addr.port}`;
  logger.info(`Listening on ${bind}`);
});

// Handle server errors
server.on('error', (error) => {
  if (error.syscall !== 'listen') {
    throw error;
  }

  const bind = typeof port === 'string' ? `Pipe ${port}` : `Port ${port}`;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      logger.error(`${bind} requires elevated privileges`);
      process.exit(1);
      break;
    case 'EADDRINUSE':
      logger.error(`${bind} is already in use`);
      process.exit(1);
      break;
    default:
      logger.info(error);
      throw error;
  }
});
