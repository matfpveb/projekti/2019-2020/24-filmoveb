const router = require('express').Router();
const directorController = require('./director.controllers');

router
  .get('/', directorController.getDirectors);

module.exports = router;
