import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {catchError} from 'rxjs/operators';
import { environment } from '../../environments/environment';
import {Director} from '../models/director.model';
import {HttpErrorHandler} from '../models/http-error-handler.model';

@Injectable({
  providedIn: 'root'
})
export class DirectorService extends HttpErrorHandler {
  directors: Observable<Array<Director>>;

  constructor(private http: HttpClient, router: Router) { 
      super(router);
      this.refreshDirectors();
  }
 
  getDirectors(): Observable<Array<Director>> { 
      return this.directors;
  }

  refreshDirectors(): void {
      this.directors = this.http
        .get<Array<Director>>(environment.server_url + '/directors')
        .pipe(catchError(super.handleError()));
  }
}
