const mongoose = require('mongoose');

const DirectorSchema = mongoose.Schema({
  name: {
    type: String,
    required: true,
    trim: true,
  },
  birthdate: Date,
  birthplace: String

});

module.exports = mongoose.model('Director', DirectorSchema);
