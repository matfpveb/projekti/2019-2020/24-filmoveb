const Director = require('./director.model');

const getDirectors = async (req, res, next) => {
  try {
    const directors = await Director.find();
    return res.json(directors);
  } catch (err) {
    return next(createError(500, 'Internal server error'));
  }
};

module.exports = {
    getDirectors
}
