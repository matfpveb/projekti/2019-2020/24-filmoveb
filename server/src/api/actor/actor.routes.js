const router = require('express').Router();
const actorController = require('./actor.controllers');

router
  .get('/', actorController.getActors);

module.exports = router;
