const router = require('express').Router();
const passport = require('passport');
const userController = require('./user.controllers');

router
  .get('/', userController.getUsers)
  .put('/watchlist', passport.authenticate('jwt', { session: false }), userController.updateWatchlist)
  .get('/watchlist', passport.authenticate('jwt', { session: false }), userController.getUserWatchlist);

module.exports = router;
