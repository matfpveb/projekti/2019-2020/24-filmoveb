import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Film } from '../models/film.model';
import { environment } from '../../environments/environment';
import { catchError, map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { HttpErrorHandler } from '../models/http-error-handler.model';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class WatchlistService extends HttpErrorHandler {

  watchlist: Observable<Array<Film>>;

  constructor(private http: HttpClient, router: Router) {
      super(router);
      this.refreshWatchlist();
    }

    getWatchlist(): Observable<Array<Film>> { 
      return this.watchlist;
  }

  refreshWatchlist(): void {
      this.watchlist = this.http
        .get<Array<Film>>(environment.server_url + '/users/watchlist')
        .pipe(catchError(super.handleError()));
  }

  updateWatchlist(filmId: string): Observable<Array<Film>> {
    return this.http.put<Array<Film>>(environment.server_url + '/users/watchlist', { id: filmId }).pipe(catchError(super.handleError()));
  }
}
