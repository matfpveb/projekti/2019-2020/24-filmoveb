import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { AuthGuard } from './_helpers/auth_guard';
import {FilmListComponent} from './film-list/film-list.component';
import {ActorListComponent} from './actor-list/actor-list.component';
import { WatchlistComponent } from './watchlist/watchlist.component';
import {DirectorListComponent} from './director-list/director-list.component';

const routes: Routes = [
  { path : 'login', component : LoginComponent },
  { path : '', component : HomeComponent, canActivate: [AuthGuard] },
  { path : 'films', component : FilmListComponent, canActivate: [AuthGuard] },
  { path : 'actors', component : ActorListComponent, canActivate: [AuthGuard] },
  { path : 'watchlist', component : WatchlistComponent, canActivate: [AuthGuard] },
  { path : 'directors', component : DirectorListComponent, canActivate: [AuthGuard] },
  { path : '**', redirectTo : '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    anchorScrolling: 'enabled'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
