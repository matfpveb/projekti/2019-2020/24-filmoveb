const createError = require('http-errors');
const jwt = require('jsonwebtoken');
const userService = require('../user/user.service');

const signIn = async (req, res, next) => {
  try {
    const user = await userService.getUserByEmail(req.body.email);
    if (!user) return next(createError(401, 'User not found'));
    const passwordOK = await user.comparePassword(req.body.password);
    if (!passwordOK) return next(createError(409, 'Invalid Password!'));
    const token = jwt.sign({ id: user.id, username: user.username }, process.env.SECRET_KEY, {
      expiresIn: 60 * 60 * 24 * 7, // 1 WEEK
    });
    return res.json({ token, userId: user._id });
  } catch (err) {
    return next(createError(500, 'Internal server error'));
  }
};

const signUp = async (req, res, next) => {
  try {
    const exist = await userService.userExist(req.body.email);
    if (exist) return next(createError(400, 'User already exist.'));
    const user = await userService.createUser(req.body);
    return res.status(201).send(user);
  } catch (err) {
    return next(createError(500, 'Internal server error'));
  }
};

module.exports = {
  signIn,
  signUp
};
