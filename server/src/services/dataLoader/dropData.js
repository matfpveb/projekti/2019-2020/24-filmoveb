require('dotenv').config({path: './src/.env'});
require('../dbConfig');
const mongoose = require('mongoose');
const logger = require('../logger');

mongoose.connection.on('connected', () => {
  mongoose.connection.dropDatabase(() => {
    logger.info('Successfully droped db')
    mongoose.connection.close();
  });
});