import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {catchError, count} from 'rxjs/operators';
import { environment } from '../../environments/environment';
import {Film} from '../models/film.model';
import {HttpErrorHandler} from '../models/http-error-handler.model';

@Injectable({
  providedIn: 'root'
})
export class FilmService extends HttpErrorHandler {
  films: Observable<Array<Film>>;
  numOfFilms: Observable<number>;

  constructor(private http: HttpClient, router: Router) { 
      super(router);
      this.refreshFilms();
  }
 
  getFilms(): Observable<Array<Film>> { 
      return this.films;
  }

  refreshFilms(): void {
      this.films = this.http
        .get<Array<Film>>(environment.server_url + '/films')
        .pipe(catchError(super.handleError()));

      this.numOfFilms = this.films.pipe(count());
  }

  addRating(filmId: string, score: number): Observable<Film> {
    return this.http.put<Film>(environment.server_url + '/films/ratings', { id: filmId, score })
                .pipe(catchError(super.handleError()));
  }

  getRandomFilm(): Observable<Film> {
    return this.http.get<Film>(environment.server_url + '/films/random')
    .pipe(catchError(super.handleError()));
  }
}
