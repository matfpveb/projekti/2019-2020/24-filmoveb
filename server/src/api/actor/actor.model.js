const mongoose = require('mongoose');

const ActorSchema = mongoose.Schema({
  name: {
    type: String,
    required: true,
    trim: true,
  },
  birthdate: String,
  birthplace: String

});

module.exports = mongoose.model('Actor', ActorSchema);
