const router = require('express').Router();
const passport = require('passport');
const movieController = require('./movie.controllers');

router
  .get('/', movieController.getMovies)
  .put('/ratings', passport.authenticate('jwt', { session: false }), movieController.addRating)
  .get('/random', movieController.getRandomMovie);

module.exports = router;
