const createError = require('http-errors');
const movieService = require('./movie.service');

const getMovies = async (req, res, next) => {
  try {
    const movies = await movieService.getMovies();
    return res.json(movies);
  } catch (err) {
    return next(createError(500, 'Internal server error'));
  }
};

const addRating = async (req, res, next) => {
  try {
    const { _id: userId } = req.user;
    const { id, score } = req.body;
    const movie = await movieService.addRating(userId, id, score);
    return res.json(movie);
  } catch (err) {
    return next(createError(500, 'Internal server error'));
  }
}

const getRandomMovie = async (req, res, next) => {
  try {
    const movie = await movieService.getRandomMovie();
    return res.json(movie[0]);
  } catch (err) {
    return next(createError(500, 'Internal server error'));
  }
}

module.exports = {
    getMovies,
    addRating,
    getRandomMovie
}
