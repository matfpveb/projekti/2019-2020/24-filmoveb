const createError = require('http-errors');
const userService = require('./user.service');

const getUsers = async (req, res, next) => {
  try {
    const users = await userService.getUsers();
    return res.json({ users });
  } catch (err) {
    return next(createError(500, 'Internal server error'));
  }
};

const updateWatchlist = async (req, res, next) => {
  try {
    const { id } = req.body;
    const { _id: userId } = req.user;
    const watchlistMovies = await userService.updateWatchlist(id, userId);
    return res.json(watchlistMovies);
  } catch (err) {
    return next(createError(500, 'Internal server error'));
  }
}

const getUserWatchlist = async (req, res, next) => {
  try {
    const { _id: userId } = req.user;
    const watchlistMovies = await userService.getUserWatchlist(userId);
    return res.json(watchlistMovies);
  } catch (err) {
    return next(createError(500, 'Internal server error'));
  }
}

module.exports = {
  getUsers,
  updateWatchlist,
  getUserWatchlist
}