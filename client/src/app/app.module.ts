import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { JwtInterceptor } from './_helpers/jwt.interceptor';
import { ErrorInterceptor } from './_helpers/error.interceptor';
import { FilmListComponent } from './film-list/film-list.component';
import { WatchlistComponent } from './watchlist/watchlist.component';
import { CallbackPipe } from './pipes/callback.pipe';
import { NgxPaginationModule } from 'ngx-pagination';
import { ActorListComponent } from './actor-list/actor-list.component';
import { SplitByLetterPipe } from './pipes/split-by-letter.pipe';
import { FilmComponent } from './film/film.component';
import { DirectorListComponent } from './director-list/director-list.component';
import { StarRatingModule } from 'angular-star-rating';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    FilmListComponent,
    WatchlistComponent,
    CallbackPipe,
    ActorListComponent,
    SplitByLetterPipe,
    FilmComponent,
    DirectorListComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    NgxPaginationModule,
    HttpClientModule,
    StarRatingModule.forRoot()
  ],
  providers: [
      { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
      { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
