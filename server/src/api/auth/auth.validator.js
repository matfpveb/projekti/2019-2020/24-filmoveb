const { Joi } = require('express-validation');

const signUpValidator = {
  body: Joi.object({
    userName: Joi.string()
      .alphanum()
      .min(3)
      .max(30)
      .required(),
    email: Joi.string()
      .email()
      .required(),
    password: Joi.string()
      .alphanum()
      .min(3)
      .required(),
  }),
};

const signInValidator = {
  body: Joi.object({
    email: Joi.string()
      .email()
      .required(),
    password: Joi.string()
      .alphanum()
      .min(8)
      .required(),
  }),
};

module.exports = {
  signUpValidator,
  signInValidator
}