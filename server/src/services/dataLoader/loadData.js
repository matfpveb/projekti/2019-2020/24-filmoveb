const fs = require('fs');
const util = require('util');
const axios = require('axios');
const logger = require('../logger');

const rf = util.promisify(fs.readFile);
const rd = util.promisify(fs.readdir);

const readDir = async dirpath => {
  try {
    const data = await rd(dirpath);
    return data;
  } catch (error) {
    logger.error(error);
  }
};

const readFile = async filepath => {
  try {
    const data = JSON.parse(await rf(filepath));
    return data;
  } catch (error) {
    logger.error(error);
  }
};

const readAllDataInDir = async dirpath => {
  const data = [];
  const allFilePaths = await readDir(dirpath);

  for (const filepath of allFilePaths) {
    const item = await readFile(dirpath + filepath);
    data.push(item);
  }

  return data;
};

const readAllMovies = async dirpath => {
  const years = await readDir(dirpath);
  const movies = [];

  for (const year of years) {
    const moviesByYear = await readAllDataInDir(dirpath + year + '/');
    movies.push(...moviesByYear);
  }

  return movies;
};

const getMoviePoster = async (title) => {
  try {
    const { Poster } = (await axios.get(`${process.env.OMDB_URI}`, { params: {t: title}})).data;
    return Poster;
  } catch (error) {
    logger.error(error);
    return null;
  }
}

module.exports = { readAllMovies, readAllDataInDir, getMoviePoster };
