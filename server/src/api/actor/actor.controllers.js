const Actor = require('./actor.model');

const getActors = async (req, res, next) => {
  try {
    const actors = await Actor.find();
    return res.json(actors);
  } catch (err) {
    return next(createError(500, 'Internal server error'));
  }
};

module.exports = {
    getActors
}
