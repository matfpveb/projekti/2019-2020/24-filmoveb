import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';
import {Film} from '../models/film.model';
import {FilmService} from '../services/film.service';
import { WatchlistService } from '../services/watchlist.service';

@Component({
  selector: 'app-film-list',
  templateUrl: './film-list.component.html',
  styleUrls: ['./film-list.component.css']
})
export class FilmListComponent implements OnInit {
  filmList: Observable<Array<Film>>;
  watchlist: Array<Film>;
  selectedGenre: string = 'all';
  currentPage: number = 1;
  numOfFilms: Observable<number>;
  search: string = "";

  constructor(filmService: FilmService, private watchlistService: WatchlistService) {
    this.filmList = filmService.getFilms();
    this.numOfFilms = filmService.numOfFilms;
    this.search = "";
  }

  isSelectedGenre(f: Film) {
    if(this.selectedGenre === 'all')
        return true;

    return f.categories.includes(this.selectedGenre);
  }

  changeGenre(g: string) {
    this.selectedGenre = g;
    this.currentPage = 1;
  }

  isSearchTermContained(f: Film) {
    let split_search: Array<string> = this.search.toLowerCase().split(" ");

    for(let s of split_search) {
        if(!f.name.toLowerCase().includes(s))
            return false;
    }

    return true;
  }

  isInWatchlist(film: Film): boolean {
    const inWatchlist = this.watchlist && this.watchlist.find(item => item._id === film._id);
    return inWatchlist ? true : false;
  }

  ngOnInit(): void {
    this.watchlistService.getWatchlist().subscribe((data) => {
      this.watchlist = data;
    });
  }

}
