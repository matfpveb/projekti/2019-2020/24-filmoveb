export class Actor {
    _id : string;
    name : string;
    birthdate : string;
    birthplace : string;
}
