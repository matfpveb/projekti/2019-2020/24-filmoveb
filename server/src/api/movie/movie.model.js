const mongoose = require('mongoose');

const MovieSchema = mongoose.Schema({
  name: {
    type: String,
    required: true,
    trim: true,
  },
  year: Number,
  runtime: Number,
  categories: [String],
  release_date: Date,
  directors: [{
    type: mongoose.Schema.ObjectId,
    ref: 'Director'
  }],
  actors: [{
    type: mongoose.Schema.ObjectId,
    ref: 'Actor'
  }],
  storyline: String,
  poster: String,
  ratings: [{
    userId: {
      type: mongoose.Schema.ObjectId,
      ref: 'User'
    },
    score: Number
  }]
});

module.exports = mongoose.model('Movie', MovieSchema);
