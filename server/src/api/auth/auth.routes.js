const router = require('express').Router();
const { validate } = require('express-validation');
const authValidator = require('./auth.validator');
const authController = require('./auth.controllers');

router
  .post('/signUp', validate(authValidator.signUpValidator), authController.signUp)
  .post('/signIn', validate(authValidator.signInValidator), authController.signIn);

module.exports = router;
