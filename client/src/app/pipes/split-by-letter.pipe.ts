import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'splitByLetter'
})
export class SplitByLetterPipe implements PipeTransform {

  transform(items: Array<any>): Array<Array<any>> {
    if(!items)
        return [items]; 

    let currentLetter = '';
    let currentGroup: Array<any> = [];
    let result: Array<Array<any>> = [];

    for(let item of items) {
        let name: string = item.name;

        if(name.charAt(0) !== currentLetter) {
            currentLetter = name.charAt(0);
            result.push(currentGroup);
            currentGroup = [];
        }
        currentGroup.push(item);
    }

    result.push(currentGroup);

    return result.filter(item => item.length > 0); 
  }

}
