export class Director {
    _id: string;
    name: string;
    birthplace: string;
    birthdate: string;
}
