import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {catchError} from 'rxjs/operators';
import { environment } from '../../environments/environment';
import {Actor} from '../models/actor.model';
import {HttpErrorHandler} from '../models/http-error-handler.model';

@Injectable({
  providedIn: 'root'
})
export class ActorService extends HttpErrorHandler {
  actors: Observable<Array<Actor>>;

  constructor(private http: HttpClient, router: Router) { 
      super(router);
      this.refreshActors();
  }
 
  getActors(): Observable<Array<Actor>> { 
      return this.actors;
  }

  refreshActors(): void {
      this.actors = this.http
        .get<Array<Actor>>(environment.server_url + '/actors')
        .pipe(catchError(super.handleError()));
  }
}
