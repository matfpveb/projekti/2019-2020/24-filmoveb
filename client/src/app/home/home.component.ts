import { Component, OnInit } from '@angular/core';
import { Film } from '../models/film.model';

import { FilmService } from '../services/film.service';
import { WatchlistService } from '../services/watchlist.service';

@Component({ templateUrl: 'home.component.html', styleUrls: ['./home.component.css'] })
export class HomeComponent implements OnInit {
    constructor(private filmService: FilmService, private watchlistService: WatchlistService) { }

    film: Film;
    watchlist: Array<Film>;

    ngOnInit(): void {
      this.filmService.getRandomFilm().subscribe(data => {
        this.film = data;
      });
      this.watchlistService.getWatchlist().subscribe(data => {
        this.watchlist = data;
      });
    }

    isInWatchlist(film: Film): boolean {
      const inWatchlist = this.watchlist && this.watchlist.find(item => item._id === film._id);
      return inWatchlist ? true : false;
    }
}
