const Movie = require('./movie.model');

const getMovies = () => Movie.find();

const addRating = async (userId, movieId, score) => {
  const movie = await Movie.findOne({ _id: movieId, 'ratings.userId': userId });
  
  // remove previous rating if there is an old one
  if (movie) {
    const ratings = movie.ratings.filter((rating) => rating.userId.toString() !== userId.toString());
    Object.assign(movie, { ratings });
    await movie.save();
  }
  
  return Movie.findByIdAndUpdate(
    { _id: movieId },
    { $push: { ratings: { userId, score } } },
    { new: true }
  )
}

const getRandomMovie = () => Movie.aggregate([{ $sample: { size: 1 } }]);

module.exports = {
  getMovies,
  addRating,
  getRandomMovie
}