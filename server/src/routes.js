const authRoutes = require('./api/auth/auth.routes');
const userRoutes = require('./api/user/user.routes');
const movieRoutes = require('./api/movie/movie.routes');
const actorRoutes = require('./api/actor/actor.routes');
const directorRoutes = require('./api/director/director.routes');

module.exports = app => {
  app.use('/auth', authRoutes);
  app.use('/users', userRoutes);
  app.use('/films', movieRoutes);
  app.use('/actors', actorRoutes);
  app.use('/directors', directorRoutes);
};
