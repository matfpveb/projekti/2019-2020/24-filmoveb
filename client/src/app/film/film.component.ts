import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Film } from '../models/film.model';
import { FilmService } from '../services/film.service';
import { WatchlistService } from '../services/watchlist.service';

@Component({
  selector: 'app-film',
  templateUrl: './film.component.html',
  styleUrls: ['./film.component.css']
})
export class FilmComponent implements OnInit {

  @Input() film: Film;
  @Input() isInWatchlist: boolean;
  @Output() deleteFromWatchlist = new EventEmitter<string>();
  globalRating: number;
  userRating: number;
  currentUser = null;
  constructor(private watchlistService: WatchlistService, private filmService: FilmService) { }

  ngOnInit(): void {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.globalRating = this.film.ratings ? this.film.ratings.reduce((total, next) => total + next.score, 0) / this.film.ratings.length : 0;
    const userRating = this.film.ratings ? this.film.ratings.find((rating) => rating.userId === this.currentUser.userId) : 0;
    this.userRating = userRating ? userRating.score : 0;
  }

  onRatingChange(event): void {
    const { rating } = event;
    this.userRating = rating;

    this.filmService.addRating(this.film._id, rating).subscribe(data => {
      this.globalRating = data.ratings.reduce((total, next) => total + next.score, 0) / data.ratings.length;
    });
  }

  onWatchlistClick(): void {
    this.watchlistService.updateWatchlist(this.film._id).subscribe(data => {
      this.isInWatchlist = !this.isInWatchlist;
      this.deleteFromWatchlist.emit(this.film._id);
    });
  }

}
