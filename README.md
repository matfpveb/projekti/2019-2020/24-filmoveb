
# Project 24-FilmoVeb

Sajt sa filmovima, njihovim opisom, glumcima, zanrovima i dodatnim podacima i funkcionalnostima

## Developers

- [Radmila Ninkovic, 1030/2018](https://gitlab.com/ninkovicr)
- [Jovan Dmitrovic, 1094/2018](https://gitlab.com/jdmitrovic)
- [Dusan Pilipovic, 1108/2018](https://gitlab.com/razzil1)


## About

Podaci o filmovima su preuzeti sa https://github.com/jsonmc/jsonmc