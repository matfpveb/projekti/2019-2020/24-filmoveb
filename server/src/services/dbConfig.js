// Mongo config
const mongoose = require('mongoose');
const logger = require('./logger');

mongoose.Promise = global.Promise;

const mongooseOptions = {
  autoIndex: true,
  useCreateIndex: true,
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false
};

mongoose.connect(process.env.MONGO_URI, mongooseOptions);

// This useCreateIndex is set only because we had a depricated warning
// mongoose.set('useCreateIndex', true);

mongoose.connection.on('error', err => {
  logger.error('🗃  Mongoose error: ', err);
});

mongoose.connection.on('connected', () => {
  logger.info('🗃  Connection to DB established successfully');
  logger.info('➖ '.repeat(22));
});
