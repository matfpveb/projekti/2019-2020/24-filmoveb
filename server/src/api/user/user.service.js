const logger = require('../../services/logger');
const User = require('./user.model');

const createUser = (user) => User.create(user);

const userExist = async (email) => await User.countDocuments({ email }) > 0;

const getUserByEmail = (email) => User.findOne({ email });

const getUsers = () => User.find();

const updateWatchlist = async (id, userId) => {
    const { watchlist } = await User.findById(userId);
    
    const { watchlist: updatedWatchlist } = await User.findByIdAndUpdate(
      userId, 
      watchlist.includes(id) ? { $pull: { watchlist: id } } : {$push: { watchlist: id }},
      { new: true }
    ).populate('watchlist');
    return updatedWatchlist;
}

const getUserWatchlist = async (userId) => {
  try {
    const { watchlist } = await User.findById(userId).populate('watchlist');
    return watchlist;
  } catch (err) {
    logger.error(err)
  }
}

module.exports = {
  createUser,
  userExist,
  getUserByEmail,
  getUsers,
  updateWatchlist,
  getUserWatchlist
}